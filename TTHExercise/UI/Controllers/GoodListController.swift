//
//  GoodListController.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

class GoodListController: BaseController {
    func getData() -> Any {
        return DataManager.shared.getGoodsList()
    }
}
