//
//  BaseController.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

// This protocol common methods for all Controllers in the application
protocol BaseController {
    // This method should use Data Manager and return data to View Controller for usage
    func getData() -> Any
}
