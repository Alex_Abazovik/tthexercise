//
//  BasketView.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class BasketView: UIView {
    // This structure is used like a plcae for all constants used for layout
    private struct Layout {
        static let viewBackgroundColor = UIColor.white
        
        static let descriptionLabelNumberOfLines = 0
        static let descriptionLabelRightConstraintConstant: CGFloat = -15.0
        static let descriptionLabelLeftConstraintConstant: CGFloat = 15.0
    }
    
    let descriptionLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.numberOfLines = Layout.descriptionLabelNumberOfLines
        return label
    }()
    
    // MARK: - Initializers
    init() {
        super.init(frame: .zero)
        
        backgroundColor = Layout.viewBackgroundColor
        addSubview(descriptionLabel)
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Update constraints
    override func updateConstraints() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        descriptionLabel.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        descriptionLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        descriptionLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Layout.descriptionLabelLeftConstraintConstant).isActive = true
        descriptionLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: Layout.descriptionLabelRightConstraintConstant).isActive = true
        
        super.updateConstraints()
    }
}
