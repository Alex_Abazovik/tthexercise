//
//  BasketViewController.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class BasketViewController: UIViewController {
    // This structure is used like a plcae for all constant strings used for layout
    private struct LayoutData {
        static let title = "BASKET"
    }
    
    //MARK: - Private Data
    private let basket: Basket
    private let customView = BasketView()
    
    //MARK: - Initializers
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(withBasket basket: Basket) {
        self.basket = basket
        customView.descriptionLabel.text = basket.getCheck()
        super.init(nibName: nil, bundle: nil)
    }
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        
        view = customView
        navigationController?.navigationBar.barStyle = .blackOpaque
        title = LayoutData.title
    }
}
