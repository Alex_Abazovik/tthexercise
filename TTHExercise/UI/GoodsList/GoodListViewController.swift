//
//  GoodListViewController.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class GoodListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // This structure is used like a plcae for all constant strings used for layout
    private struct LayoutData {
        static let title = "GOODS LIST"
    }
    
    //MARK: - Private data
    private var appCoordinator: AppCoordinator
    private var controller: BaseController
    private var customView = GoodListView()
    private var data = [Good]()
    private var basket: Basket = Basket()
    
    //MARK: - Initializers
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(withCoordinator coordinator: AppCoordinator, andController controller: BaseController) {
        self.appCoordinator = coordinator
        self.controller = controller
        super.init(nibName: nil, bundle: nil)
    }
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView.tableView.dataSource = self
        customView.tableView.delegate = self
        
        customView.goToBasketButton.addTarget(self, action: #selector(goToBasketButtonAction(_:)), for: .touchUpInside)
    }
    
    override func loadView() {
        super.loadView()
        
        view = customView
        navigationController?.navigationBar.barStyle = .blackOpaque
        title = LayoutData.title
        
        if let goodsArray = controller.getData() as? [Good] {
            data = goodsArray
        }
    }
    
    // MARK: - Private methods
    @objc private func goToBasketButtonAction(_ sender: UIButton) {
        appCoordinator.pushBasketVC(withBasket: basket)
    }
}


//MARK: - UITableViewDelegate, UITableViewDataSource
extension GoodListViewController {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellIdentifier.goodList.rawValue)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: TableViewCellIdentifier.goodList.rawValue)
        }
        let currentGood = data[indexPath.row]
        
        cell?.textLabel?.text = currentGood.getName()
        cell?.detailTextLabel?.text = currentGood.getPrice().formattedFloat() + " $"
        cell?.selectionStyle = .none
        
        // If basket contain the current good accessoryType should be changed
        basket.checkContaining(good: currentGood) ? (cell?.accessoryType = .checkmark) : (cell?.accessoryType = .none)
        
        // If basket is empty GoToBasket button should be blocked
        basket.isEmpty() ? (customView.goToBasketButton.isEnabled = false) : (customView.goToBasketButton.isEnabled = true)
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentGood = data[indexPath.row]
        if basket.checkContaining(good: currentGood) {
            basket.remove(good: currentGood)
        } else {
            basket.add(good: currentGood)
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}
