//
//  GoodListView.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import UIKit

class GoodListView: UIView {
    // This structure is used like a plcae for all constants used for layout
    private struct Layout {
        static let goToBasketButtonBackgroundColor = UIColor.black.withAlphaComponent(0.7)
        static let goToBasketButtonTintColor = UIColor.yellow
        
        static let tableViewTopConstraintConstant: CGFloat = 15.0
        static let tableViewRightConstraintConstant: CGFloat = -15.0
        static let tableViewBottomConstraintConstant: CGFloat = -15.0
        static let tableViewLeftConstraintConstant: CGFloat = 15.0
        
        static let goToBasketButtonLeftConstraintConstant: CGFloat = 15.0
        static let goToBasketButtonRightConstraintConstant: CGFloat = -15.0
        static let goToBasketButtonBottomConstraintConstant: CGFloat = -30.0
        static let goToBasketButtonHeightConstraintConstant: CGFloat = 44.0
    }
    
    // This structure is used like a plcae for all constant strings used for layout
    private struct LayoutData {
        static let goToBasketButtonTitle = "GO TO BASKET"
    }
    
    // MARK: - Public data
    let tableView: UITableView = {
        let tv = UITableView(frame: CGRect.zero, style: .plain)
        tv.allowsMultipleSelection = true
        return tv
    }()
    
    let goToBasketButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = Layout.goToBasketButtonBackgroundColor
        button.tintColor = Layout.goToBasketButtonTintColor
        button.setTitle(LayoutData.goToBasketButtonTitle, for: .normal)
        button.isEnabled = false
        return button
    }()
    
    // MARK: - Initializers
    init() {
        super.init(frame: .zero)
        
        addSubview(tableView)
        addSubview(goToBasketButton)
        
        setNeedsUpdateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Update constraints
    override func updateConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        goToBasketButton.translatesAutoresizingMaskIntoConstraints = false
        
        tableView.topAnchor.constraint(equalTo: topAnchor, constant: Layout.tableViewTopConstraintConstant).isActive = true
        tableView.rightAnchor.constraint(equalTo: rightAnchor, constant: Layout.tableViewRightConstraintConstant).isActive = true
        tableView.bottomAnchor.constraint(equalTo: goToBasketButton.topAnchor, constant: Layout.tableViewBottomConstraintConstant).isActive = true
        tableView.leftAnchor.constraint(equalTo: leftAnchor, constant: Layout.tableViewLeftConstraintConstant).isActive = true
        
        goToBasketButton.leftAnchor.constraint(equalTo: leftAnchor, constant: Layout.goToBasketButtonLeftConstraintConstant).isActive = true
        goToBasketButton.rightAnchor.constraint(equalTo: rightAnchor, constant: Layout.goToBasketButtonRightConstraintConstant).isActive = true
        goToBasketButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: Layout.goToBasketButtonBottomConstraintConstant).isActive = true
        goToBasketButton.heightAnchor.constraint(equalToConstant: Layout.goToBasketButtonHeightConstraintConstant).isActive = true
        
        super.updateConstraints()
    }
}
