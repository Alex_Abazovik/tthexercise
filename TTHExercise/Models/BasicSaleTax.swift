//
//  BasicSaleTax.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

class BasicSaleTax: GoodDecorator {
    
    override func getName() -> String {
        return super.getName()
    }
    
    override func getPrice() -> Float {
        return super.getPrice()
    }
    
    override func getTaxInCash() -> Float {
        let value = getPrice() * Tax.basicSalesTax.rawValue
        return super.getTaxInCash() + (value / GoodsPreferences.roundingAccuracy).rounded(.up) * GoodsPreferences.roundingAccuracy
    }
    
    override func getPriceWithTax() -> Float {
        return getPrice() + getTaxInCash()
    }
}
