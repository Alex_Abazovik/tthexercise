//
//  Basket.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

class Basket {
    
    // This options are used for creating total check string
    private struct CheckOptions {
        static let countSymbol = "1 "
        static let dividerBetweenNameAndPrice = ": "
        static let salesTaxesTitle = "Sales Taxes: "
        static let totalTitle = "Total: "
    }
    
    // MARK: - Private data
    // Goods array contains all goods containng in basket object currently
    private var goods: [Good]
    
    // MARK: - Initializers
    required init() {
        goods = [Good]()
    }
    
    // MARK: - Public methods
    // This method add new Good to the array of Goods which represent current basket
    public func add(good: Good) {
        goods.append(good)
    }
    
    // This method check if the good already in array of goods / basket
    public func checkContaining(good: Good) -> Bool {
        return goods.contains(where: { $0.getName() == good.getName() })
    }
    
    // This method delete Good from array / basket. In current realization name of Good is used to identify which product should be deleted
    public func remove(good: Good) {
        guard let index = goods.firstIndex(where: { $0.getName() == good.getName() }) else { return }
        goods.remove(at: index)
    }
    
    // This method return readable representation of current basket content
    public func getCheck() -> String {
        var completedString: String = ""
        goods.forEach { good in
            completedString.append(CheckOptions.countSymbol + good.getName() + CheckOptions.dividerBetweenNameAndPrice + good.getPriceWithTax().formattedFloat() + "\n")
        }
        completedString.append("\n" + CheckOptions.salesTaxesTitle + calculateTotalTax().formattedFloat() + "\n\n")
        completedString.append(CheckOptions.totalTitle + calculateTotalPrice().formattedFloat())
        
        return completedString
    }
    
    // This method return boolean variable that represent if any Goods are in basket
    func isEmpty() -> Bool {
        return goods.isEmpty
    }
    
    // MARK: - Private mathods
    // This method return the value represent total price of all Goods in basket
    private func calculateTotalPrice() -> Float {
        var totalPrice: Float = 0.0
        goods.forEach({ totalPrice += $0.getPriceWithTax() })
        return totalPrice
    }
    
    // This method return the value represent total tax of all Goods in basket
    private func calculateTotalTax() -> Float {
        var totalTax: Float = 0.0
        goods.forEach({ totalTax += $0.getTaxInCash() })
        return totalTax
    }
}
