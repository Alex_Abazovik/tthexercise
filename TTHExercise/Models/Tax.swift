//
//  Tax.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

enum Tax: Float {
    // Tax free
    case free = 0.0
    // // Equivalent 10%
    case basicSalesTax = 0.1
    // Equivalent 5%
    case additionalSalesTax = 0.05
}
