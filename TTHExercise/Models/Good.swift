//
//  Good.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

protocol Good {
    // Return the value that contain current name of good. In some cases can contain an additional prefix (for example "Imported").
    func getName() -> String
    
    // Return the value thet represent current good price without any additional taxes.
    func getPrice() -> Float
    
    // Return the value that represent current tax of good depending on type of item.
    func getTaxInCash() -> Float
    
    // Return the value that contain current price of good with all additional taxes
    func getPriceWithTax() -> Float
}

struct GoodsPreferences {
    // This value is used for rounding calculated tax
    static let roundingAccuracy: Float = 0.05
}
