//
//  ImportedGoodTax.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

class ImportedGoodTax: GoodDecorator {
    override func getName() -> String {
        return GoodsNamePrefixes.Imported.rawValue + super.getName()
    }
    
    override func getPrice() -> Float {
        return super.getPrice()
    }
    
    override func getTaxInCash() -> Float {
        let value = getPrice() * Tax.additionalSalesTax.rawValue
        return super.getTaxInCash() + (value / GoodsPreferences.roundingAccuracy).rounded(.up) * GoodsPreferences.roundingAccuracy
    }
    
    override func getPriceWithTax() -> Float {
        return getPrice() + getTaxInCash()
    }
}
