//
//  TaxFreeGood.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

final class TaxFreeGood: Good {
    var name: String
    var price: Float
    
    init(withName name: String, andPrice price: Float) {
        self.name = name
        self.price = price
    }
    
    func getName() -> String {
        return name
    }
    
    func getPrice() -> Float {
        return price
    }
    
    func getTaxInCash() -> Float {
        let value = getPrice() * Tax.free.rawValue
        return (value / GoodsPreferences.roundingAccuracy).rounded() * GoodsPreferences.roundingAccuracy
    }
    
    func getPriceWithTax() -> Float {
        return getPrice() + getTaxInCash()
    }
}
