//
//  GoodDecorator.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

class GoodDecorator: Good {
    let good: Good
    
    init(withGood good: Good) {
        self.good = good
    }
    
    func getName() -> String {
        return good.getName()
    }
    
    func getPrice() -> Float {
        return good.getPrice()
    }
    
    func getTaxInCash() -> Float {
        return good.getTaxInCash()
    }
    
    func getPriceWithTax() -> Float {
        return good.getPriceWithTax()
    }
}
