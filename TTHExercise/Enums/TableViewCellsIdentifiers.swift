//
//  TableViewCellsIdentifiers.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

enum TableViewCellIdentifier: String {
    case goodList = "GoodListTableViewCellIdentifier"
}
