//
//  Float+FormattedString.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import Foundation

extension Float {
    // This method return the float value with specific number of digits after comma. This value can be transferred as parameter or will be used 2 as a default value
    func formattedFloat(withNumberOfDigitsAfterPoint value: Int = 2) -> String {
        return String(format: "%.\(value)f", self)
    }
}
