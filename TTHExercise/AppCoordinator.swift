//
//  AppCoordinator.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//
import UIKit

// This object is used like Dependency container and main control point in application
class AppCoordinator {
    // MARK: - Private data
    private var navigationController: UINavigationController?
    
    // MARK: - Public data
    // This object is used as a main window in Application Delegate
    let window: UIWindow
    
    // MARK: - Initializers
    init(withWindow window: UIWindow) {
        self.window = window
        window.backgroundColor = .white
    }
    
    // In this method is created GoodListViewController as start view controller and placed as root view controller in the navigation stack
    func start() {
        let goodListController = GoodListController()
        let goodListViewController = GoodListViewController(withCoordinator: self, andController: goodListController)
        
        navigationController = UINavigationController(rootViewController: goodListViewController)
        window.rootViewController = navigationController
        
        window.makeKeyAndVisible()
    }
    
    // This method is used for creating new Basket view controller and push it to the navigation stack
    func pushBasketVC(withBasket basket: Basket) {
        guard let navigationController = navigationController else { return }
        
        let basketViewController = BasketViewController(withBasket: basket)
        navigationController.pushViewController(basketViewController, animated: true)
    }
}
