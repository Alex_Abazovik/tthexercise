//
//  DataManager.swift
//  TTHExercise
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

// This class is Singleton that represent data manager which help to get data from all points of application
class DataManager {
    static let shared = DataManager()
    
    // This initializer should be private to disable creation additional objects
    private init() { }
    
    // This method shoild be used to get data from remote or local storage
    public func getGoodsList() -> [Good] {
        return configureMockArray()
    }
    
    // In current realization is used mocked array of Goods
    private func configureMockArray() -> [Good] {
        return [GoodObjects.bagOfSkittles, GoodObjects.walkman, GoodObjects.bagOfMicrowavePopcorn, GoodObjects.importedBagOfVanillaHazelnutCoffee, GoodObjects.importedVespa, GoodObjects.importedCrateOfAlmondSnickers, GoodObjects.discman, GoodObjects.importedBottleOfWine, GoodObjects.bagOfFairTradeCoffee]
    }
}

struct GoodObjects {
    static let bagOfSkittles = TaxFreeGood(withName: "16lb bag of Skittles", andPrice: 16.0)
    static let walkman = BasicSaleTax(withGood: TaxFreeGood(withName: "Walkman", andPrice: 99.99))
    static let bagOfMicrowavePopcorn = TaxFreeGood(withName: "bag of microwave Popcorn", andPrice: 0.99)
    static let importedBagOfVanillaHazelnutCoffee = ImportedGoodTax(withGood: TaxFreeGood(withName: "bag of Vanilla-Hazelnut Coffee", andPrice: 11.0))
    static let importedVespa = ImportedGoodTax(withGood: BasicSaleTax(withGood: TaxFreeGood(withName: "Vespa", andPrice: 15001.25)))
    static let importedCrateOfAlmondSnickers = ImportedGoodTax(withGood: TaxFreeGood(withName: "crate of Almond Snickers", andPrice: 75.99))
    static let discman = BasicSaleTax(withGood: TaxFreeGood(withName: "Discman", andPrice: 55.00))
    static let importedBottleOfWine = ImportedGoodTax(withGood: BasicSaleTax(withGood: TaxFreeGood(withName: "bottle of Wine", andPrice: 10.00)))
    static let bagOfFairTradeCoffee = TaxFreeGood(withName: "300# Bag of Fair-Trade Coffee", andPrice: 997.99)
}
