//
//  TTHExerciseTests.swift
//  TTHExerciseTests
//
//  Created by Alex on 2/26/19.
//  Copyright © 2019 Alex. All rights reserved.
//

import XCTest
@testable import TTHExercise

enum TaxForTesting: Float {
    case free = 0.0
    case basicSalesTax = 10.0
    case additionalSalesTax = 5.0
}

class TTHExerciseTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testTaxFreeOGoodCreating() {
        let goodPrice: Float = 20.0
        let inputGoodName = "Tax free good"
        let outputGoodName = inputGoodName
        let calculatedTaxInCash = goodPrice * (TaxForTesting.free.rawValue / 100)
        let priceWithTax = goodPrice + calculatedTaxInCash
        
        let good = TaxFreeGood(withName: inputGoodName, andPrice: goodPrice)
        
        XCTAssertEqual(good.getName(), outputGoodName, "Good getName method returned incorrect value.")
        XCTAssertEqual(good.getPrice(), goodPrice, "Good getPrice method returned incorrect value.")
        XCTAssertEqual(good.getTaxInCash(), calculatedTaxInCash, "Good getTaxInCash method returned incorrect value.")
        XCTAssertEqual(good.getPriceWithTax(), priceWithTax, "Good getPriceWithTax method returned incorrect value.")
    }
    
    func testBasicSaleTaxGoodCreating() {
        let goodPrice: Float = 20.0
        let inputGoodName = "Good with basic sale tax"
        let outputGoodName = inputGoodName
        let calculatedTaxInCash = goodPrice * ((TaxForTesting.free.rawValue + TaxForTesting.basicSalesTax.rawValue) / 100)
        let priceWithTax = goodPrice + calculatedTaxInCash
        
        let good = BasicSaleTax(withGood: TaxFreeGood(withName: inputGoodName, andPrice: goodPrice))
        
        XCTAssertEqual(good.getName(), outputGoodName, "Good getName method returned incorrect value.")
        XCTAssertEqual(good.getPrice(), goodPrice, "Good getPrice method returned incorrect value.")
        XCTAssertEqual(good.getTaxInCash(), calculatedTaxInCash, "Good getTaxInCash method returned incorrect value.")
        XCTAssertEqual(good.getPriceWithTax(), priceWithTax, "Good getPriceWithTax method returned incorrect value.")
    }
    
    func testImportedBasicSaleTaxGoodCreating() {
        let goodPrice: Float = 20.0
        let inputGoodName = "good with basic sale tax"
        let outputGoodName = "Imported good with basic sale tax"
        let calculatedTaxInCash = goodPrice * ((TaxForTesting.free.rawValue + TaxForTesting.basicSalesTax.rawValue + TaxForTesting.additionalSalesTax.rawValue) / 100)
        let priceWithTax = goodPrice + calculatedTaxInCash
        
        let good = ImportedGoodTax(withGood: BasicSaleTax(withGood: TaxFreeGood(withName: inputGoodName, andPrice: goodPrice)))
        
        XCTAssertEqual(good.getName(), outputGoodName, "Good getName method returned incorrect value.")
        XCTAssertEqual(good.getPrice(), goodPrice, "Good getPrice method returned incorrect value.")
        XCTAssertEqual(good.getTaxInCash(), calculatedTaxInCash, "Good getTaxInCash method returned incorrect value.")
        XCTAssertEqual(good.getPriceWithTax(), priceWithTax, "Good getPriceWithTax method returned incorrect value.")
    }
    
    func testImportedTaxFreeGoodCreating() {
        let goodPrice: Float = 20.0
        let inputGoodName = "tax free good"
        let outputGoodName = "Imported tax free good"
        let calculatedTaxInCash = goodPrice * ((TaxForTesting.free.rawValue + TaxForTesting.additionalSalesTax.rawValue) / 100)
        let priceWithTax = goodPrice + calculatedTaxInCash
        
        let good = ImportedGoodTax(withGood: TaxFreeGood(withName: inputGoodName, andPrice: goodPrice))
        
        XCTAssertEqual(good.getName(), outputGoodName, "Good getName method returned incorrect value.")
        XCTAssertEqual(good.getPrice(), goodPrice, "Good getPrice method returned incorrect value.")
        XCTAssertEqual(good.getTaxInCash(), calculatedTaxInCash, "Good getTaxInCash method returned incorrect value.")
        XCTAssertEqual(good.getPriceWithTax(), priceWithTax, "Good getPriceWithTax method returned incorrect value.")
    }
    
    func testPriceWithTaxTaxDifferentPriceValueSizes() {
        let inputPrices: [Float] = [0.99, 2.0, 90.0, 103.87, 1234.25, 2456.76, 12352.81]
        let outputPrices: [Float] = [1.14, 2.3, 103.5, 119.47, 1419.45, 2825.31, 14205.76]
        let goodName = "Good name"
        
        for (index, element) in inputPrices.enumerated() {
            let good = ImportedGoodTax(withGood: BasicSaleTax(withGood: TaxFreeGood(withName: goodName, andPrice: element)))
            XCTAssertEqual(good.getPriceWithTax(), outputPrices[index], "Good getPriceWithTax method return incorrect value.")
        }
    }
    
    func testBasketOne() {
        // ------ Input ------
        // 1 16lb bag of Skittles at 16.00
        // 1 Walkman at 99.99
        // 1 bag of microwave Popcorn at 0.99
        
        // ------- Output -------
        // 1 16lb bag of Skittles: 16.00
        // 1 Walkman: 109.99
        // 1 bag of microwave Popcorn: 0.99
        
        // Sales Taxes: 10.00
        
        // Total: 126.98
        
        let outputString = "1 16lb bag of Skittles: 16.00\n1 Walkman: 109.99\n1 bag of microwave Popcorn: 0.99\n\nSales Taxes: 10.00\n\nTotal: 126.98"
        
        let basket = Basket()
        basket.add(good: GoodObjects.bagOfSkittles)
        basket.add(good: GoodObjects.walkman)
        basket.add(good: GoodObjects.bagOfMicrowavePopcorn)
        
        XCTAssertEqual(basket.getCheck(), outputString, "Good getCheck return incorrect value.")
    }
    
    func testBasketTwo() {
        // ----- Input -----
        // 1 imported bag of Vanilla-Hazelnut Coffee at 11.00
        // 1 Imported Vespa at 15,001.25

        // ------ Output ------
        // 1 imported bag of Vanilla-Hazelnut Coffee: 11.55
        // 1 Imported Vespa: 17,251.5
        
        // Sales Taxes: 2,250.8
        
        // Total: 17,263.05
        
        let outputString = "1 Imported bag of Vanilla-Hazelnut Coffee: 11.55\n1 Imported Vespa: 17251.50\n\nSales Taxes: 2250.80\n\nTotal: 17263.05"
        
        let basket = Basket()
        basket.add(good: GoodObjects.importedBagOfVanillaHazelnutCoffee)
        basket.add(good: GoodObjects.importedVespa)
        
        XCTAssertEqual(basket.getCheck(), outputString, "Good getCheck return incorrect value.")
    }
    
    func testBasketThree() {
        // ----- Input -----
        // 1 imported crate of Almond Snickers at 75.99
        // 1 Discman at 55.00
        // 1 Imported Bottle of Wine at 10.00
        // 1 300# bag of Fair-Trade Coffee at 997.99

        
        // ------ Output ------
        // 1 imported crate of Almond Snickers: 79.79
        // 1 Discman: 60.5
        // 1 imported bottle of Wine: 11.5
        // 1 300# Bag of Fair-Trade Coffee: 997.99
        
        // Sales Taxes: 10.8
        
        // Total: 1,149.78
        
        let outputString = "1 Imported crate of Almond Snickers: 79.79\n1 Discman: 60.50\n1 Imported bottle of Wine: 11.50\n1 300# Bag of Fair-Trade Coffee: 997.99\n\nSales Taxes: 10.80\n\nTotal: 1149.78"
        
        let basket = Basket()
        basket.add(good: GoodObjects.importedCrateOfAlmondSnickers)
        basket.add(good: GoodObjects.discman)
        basket.add(good: GoodObjects.importedBottleOfWine)
        basket.add(good: GoodObjects.bagOfFairTradeCoffee)
        
        XCTAssertEqual(basket.getCheck(), outputString, "Good getCheck return incorrect value.")
    }
}
