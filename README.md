# TTHExercise

## Usage instructions:

1. When you launch the Application you will see the screen with preloaded list of products. 
2. You can select/deselect any product from this predefined list. 
3. Selected products will be added to your basket automatically and marked with tick.
4. When at list one product is selected you will have ability to tap on “GO TO BASKET” button that will bring you to the Basket screen with the total bill’s details. 

### Selected architecture review:

In this project I used MVC architecture to make it easy to understand. But common architecture were improved by adding additional layer - AppCoordinator. This layer is used as Dependency container and main control point in the Application. All dependencies are created in this layer. It makes code even more divided into modules. These modules can be easy switched to another ones, so we can easy change any logic or UI if needed. 

And here is why Decorator pattern was used in Model Layer. I guess this way gives the opportunity to easy add additional Taxes into project without multiple inheritance. 

### UnitTesting:

I tried to cover most important cases in UnitTesting such as correct creating Goods of all types and correct price counting. But in one case I got fail and want to describe why it was. 

Read business rules and saw next row: 

- “Sales tax is rounded up to the nearest multiple of $0.05. This rounding is done by item, by type of tax (basic sales and import duty)”

So. Try to calculate tax for second basket. Imported bag of coffee - it is 5% tax,  with price - 11.0 dollars. It is 0.55 cents of tax without additional rounding. After that we have Vespa with price - 15001.25 and tax - 15%. So tax will be - 2250.1875 and using rounded rule - 2250.20. So even in this point we have difference with outputs in task (2250.8).

